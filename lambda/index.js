exports.handler = (event, context, callback) => {

    const response = {
        statusCode: 200,
        body: JSON.stringify("LAMBDA!  " +event.multiValueHeaders["User-Agent"]),
    };
    callback(null, response);
};

