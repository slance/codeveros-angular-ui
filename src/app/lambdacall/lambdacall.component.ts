import { Injectable, Component, OnInit, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
    selector: 'codeveros-lambdacall-root',
    templateUrl: './lambdacall.component.html',
    styleUrls: ['./lambdacall.component.css']
})


@Injectable()
export class LambdacallComponent implements OnInit {
    constructor(private http: HttpClient) { }
    restoutput: Observable<string>;
    url = 'https://iqyfvyn6r4.execute-api.us-east-1.amazonaws.com/dev/microservice';
    ngOnInit() {
        this.restoutput = this.http.get<string>(this.url);
    }
}

